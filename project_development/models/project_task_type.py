from odoo import fields, models


class ProjectTaskType(models.Model):
    _inherit = "project.task.type"

    stage_type = fields.Selection(
        selection=[
            ("draft", "Draft"),
            ("open", "In Progress"),
            ("closed", "Closed"),
        ],
        default="open",
    )
