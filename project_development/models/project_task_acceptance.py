from odoo import api, fields, models


class ProjectTaskAcceptance(models.Model):
    _name = "project.task.acceptance"
    _description = "Project Task Acceptance"

    name = fields.Char()
    developer_ok = fields.Boolean()
    reviewer_ok = fields.Boolean()
    task_id = fields.Many2one(
        comodel_name="project.task",
        required=True,
    )
    owned = fields.Boolean(
        compute="_compute_owned",
    )
    review = fields.Boolean(
        compute="_compute_review",
    )

    @api.depends("task_id.user_id")
    def _compute_owned(self):
        for acceptance in self:
            acceptance.owned = acceptance.task_id.user_id == self.env.user

    @api.depends("task_id.reviewer_id")
    def _compute_review(self):
        for acceptance in self:
            acceptance.review = acceptance.task_id.reviewer_id == self.env.user
