from odoo import fields, models


class ProjectTask(models.Model):
    _inherit = "project.task"

    description = fields.Html()
    repo = fields.Char()
    pull_request = fields.Char()
    module = fields.Char()
    sprint_id = fields.Many2one(
        comodel_name="project.sprint",
    )
    reviewer_id = fields.Many2one(
        comodel_name="res.users",
        default=lambda self: self.env.user,
    )
    acceptance_criteria_id = fields.One2many(
        comodel_name="project.task.acceptance",
        inverse_name="task_id",
        tracking=True,
    )
    observation_ids = fields.One2many(
        comodel_name="project.task.observation",
        inverse_name="task_id",
        tracking=True,
    )
    stage_type = fields.Selection(
        related="stage_id.stage_type",
    )
