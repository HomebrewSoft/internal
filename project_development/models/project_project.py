from odoo import fields, models


class ProjectProject(models.Model):
    _inherit = "project.project"

    odoo_version = fields.Char()
    master_repo = fields.Char()
    domain = fields.Char()
    dev_url = fields.Char()
    test_url = fields.Char()
    prod_url = fields.Char()
    dev_server_admin_id = fields.Many2one(
        comodel_name="res.users",
    )
    test_server_admin_id = fields.Many2one(
        comodel_name="res.users",
    )
    prod_server_admin_id = fields.Many2one(
        comodel_name="res.users",
    )
