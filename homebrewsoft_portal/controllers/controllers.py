"""
Module for the controllers of the HomebrewSoft portal
"""

from odoo import http
from odoo.http import request


class HomebrewSoftPortal(http.Controller):
    """
    Class that extends from controller to create routes and render the portal templates
    """

    @http.route('/inicio', type='http', auth='public', website=True)
    def home(self):
        """
        Route for the main page
        """
        return request.render('homebrewsoft_portal.home_page')

    @http.route('/servicios', type='http', auth='public', website=True)
    def services(self):
        """
        Route for the services page
        """
        return request.render('homebrewsoft_portal.services_page')

    @http.route('/contacto', type='http', auth='public', website=True)
    def contact(self):
        """
        Route for the contact page
        """
        return request.render('homebrewsoft_portal.contact_page')

    @http.route('/nosotros', type='http', auth='public', website=True)
    def aboutus(self):
        """
        Route for the about us page
        """
        return request.render('homebrewsoft_portal.aboutus_page')
